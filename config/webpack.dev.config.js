const commonPaths = require('./common-paths');

console.log("ROOT" + commonPaths.root);

const config = {
    devtool: 'inline-source-map',
    devServer: {
        contentBase: commonPaths.outputPath,
        compress: false,
        historyApiFallback: true,
        hot: true,
        port: 9000
    }
};

module.exports = config;
