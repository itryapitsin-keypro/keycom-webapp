var exec = require('child_process').exec;
var execSync = require('child_process').execSync;
var removeEmptyLines = require('./remove-empty-lines');

module.exports = function (command, callback) {
    var hgCommand = [
        'hg',
        command
    ].join(' ');

    if (callback) {
        exec(hgCommand, function (err, stdout) {
            if (err) { return callback(err) }

            callback(null, removeEmptyLines(stdout))
        })
    } else {
        return removeEmptyLines('' + execSync(hgCommand))
    }
};
