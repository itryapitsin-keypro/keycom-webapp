/**
 * REST API representation of Address
 * @interface Address
 */
export interface Address {
    /**
     * Record id
     * @type {string}
     * @memberof Address
     */
    id?: string;

    /**
     * City part of address
     * @type {string}
     * @memberof Address
     */
    city: string;

    lat: number;

    lon: number;

    name: string;

    sourceId?: string;

    streetName: string;

    streetNo: string;
}
