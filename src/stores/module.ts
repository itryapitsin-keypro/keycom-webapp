import { ContainerModule, interfaces } from 'inversify';
import { QuickAddressSearchWindowStore } from './windows/quick-address-search-window-store';

export const StoreModule = new ContainerModule((bind: interfaces.Bind): void => {
    bind(QuickAddressSearchWindowStore)
        .toSelf()
        .inSingletonScope();
});
