import 'reflect-metadata';
import { action, computed, observable } from 'mobx';
import { injectable } from 'inversify';
import { Address } from '../../api/models/address';

@injectable()
export class QuickAddressSearchWindowStore {
    @observable private _selectedItem: Address;

    @observable private _items: Address[] = [];

    @observable private _showing: boolean = false;

    @observable private _loading: boolean = false;

    @observable private _pattern: string = '';

    @action public setItems = (items: Address[]): void => {
        this._items = items;
    };

    @action public show = (): void => {
        this._showing = true;
    };

    @action public hide = (): void => {
        this._showing = false;
    };

    @action public startLoading = (): void => {
        this._loading = true;
    };

    @action public stopLoading = (): void => {
        this._loading = false;
    };

    @action public selectItem = (item: Address): void => {
        this._selectedItem = item;
        this._pattern = item.name;
    };

    @action public setPatter = (pattern: string): void => {
        this._pattern = pattern;
    };

    @computed public get pattern(): string {
        return this._pattern;
    }

    @computed public get isShow(): boolean {
        return this._showing;
    }

    @computed public get isLoading(): boolean {
        return this._loading;
    }

    @computed public get items(): Address[] {
        return this._items;
    }

    @computed public get selectedItem(): Address {
        return this._selectedItem;
    }
}
