export interface ILauncher {
    (): void;

    name?: string;
}
