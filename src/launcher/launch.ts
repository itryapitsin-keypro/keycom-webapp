import { PortalEmbeddedApp } from './portal-embedded-app';
import { SelfHostApp } from './self-host-app';

type START_MODE = 'portal' | 'mobile' | 'normal';

function getModeToStart(): START_MODE {
    if (document.body.classList.contains('keygwt')) {
        return 'portal';
    }

    const urlSearchParams = new URLSearchParams(window.location.search);
    if (urlSearchParams.get('view') === 'mobile') {
        return 'mobile';
    }

    return 'normal';
}

export default (): void => {
    switch (getModeToStart()) {
        case 'mobile':
            break;
        case 'portal':
            PortalEmbeddedApp();
            break;
        case 'normal':
            SelfHostApp();
            break;
    }
};
