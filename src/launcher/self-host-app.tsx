import * as React from 'react';
import { Container } from 'inversify';
import * as ReactDOM from 'react-dom';

import { TopBarController } from '../controllers/top-bar';
import { QuickAddressSearchWindowController } from '../controllers/windows/quick-address-search-window';
import { StoreModule } from '../stores/module';
import { ApiModule } from '../api/module';
import { ControllerModule } from '../controllers/module';
import { ILauncher } from './launcher';
import { App } from '../components/app/app';

// loading component for suspense fallback
const Loader: React.FC<{}> = (): React.ReactElement => (
    <div className="app">
        <div>loading...</div>
    </div>
);

export const SelfHostApp: ILauncher = (): void => {
    const container = new Container();
    container.load(StoreModule, ApiModule, ControllerModule);

    const topBarController = container.get(TopBarController);
    const quickAddressSearchWindowController = container.get(QuickAddressSearchWindowController);

    ReactDOM.render(
        <React.Suspense fallback={<Loader />}>
            <App
                quickAddressSearchWindowController={quickAddressSearchWindowController}
                topBarController={topBarController}
            />
        </React.Suspense>,
        document.querySelector('#app'),
    );
};
