import { IThemedProps } from '../props/themed-props';

export interface IWindowProps extends IThemedProps {
    closeAction?: () => void;

    isShow?: boolean;
}
