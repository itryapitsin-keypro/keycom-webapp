import React from 'react';

import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';
import { QuickAddressSearchWindow } from './component';
import { Container } from 'inversify';
import { StoreModule } from '../../../stores/module';
import { ApiModule } from '../../../api/module';
import { ControllerModule } from '../../../controllers/module';
import { QuickAddressSearchWindowController } from '../../../controllers/windows/quick-address-search-window';

let container: HTMLDivElement | null = null;
beforeEach((): void => {
    // setup a DOM element as a render target
    container = document.createElement('div');
    document.body.appendChild(container);
});

afterEach((): void => {
    // cleanup on exiting
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it('testing QuickAddressSearchWindow component', async (): Promise<void> => {
    const di = new Container();
    di.load(StoreModule, ApiModule, ControllerModule);
    const quickAddressSearchWindowController = di.get(QuickAddressSearchWindowController);

    act((): void => {
        const component = (
            <QuickAddressSearchWindow
                blockName="quick-address-search"
                controller={quickAddressSearchWindowController}
            />
        );
        render(component, container);
    });
    expect(container.childNodes.length).toBe(0); // Hidden window by default

    quickAddressSearchWindowController.quickAddressSearchWindowStore.show();
    act((): void => {
        const component = (
            <QuickAddressSearchWindow
                blockName="quick-address-search"
                controller={quickAddressSearchWindowController}
            />
        );
        render(component, container);
    });
    expect(container.childNodes.length).toBe(1); // Available window after changing state from controller
});
