import React from 'react';
import { ClassNameFormatter } from '@bem-react/classname';
import { observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';

import { IQuickAddressSearchWindowProps } from './props';
import { ControllerComponent } from '../../controller-component';
import { QuickAddressSearchWindowController } from '../../../controllers/windows/quick-address-search-window';
import {
    Window,
    WindowBody,
    WindowHeader,
    WindowHeaderButtonsContainer,
    WindowHeaderClose,
    WindowHeaderFavorite,
    WindowHeaderMinimize,
} from '../base-window';
import { className } from '../../../utils';
import { Address } from '../../../api/models/address';

/**
 *
 *
 * @class Component
 * @extends {ControllerComponent<IQuickAddressSearchWindowProps, QuickAddressSearchWindowController>}
 */
@observer
class Component extends ControllerComponent<
    IQuickAddressSearchWindowProps,
    QuickAddressSearchWindowController
> {
    private readonly searchInputFormatter: ClassNameFormatter;
    private readonly locateBtnFormatter: ClassNameFormatter;
    private readonly addressesDropdownFormatter: ClassNameFormatter;
    private readonly titleFormatter: ClassNameFormatter;
    private readonly closeBtnFormatter: ClassNameFormatter;

    public constructor(props: IQuickAddressSearchWindowProps) {
        super(props);

        this.titleFormatter = className(this.blockName, 'modal-title');
        this.closeBtnFormatter = className(this.blockName, 'close');

        this.searchInputFormatter = className(this.blockName, 'search-input');
        this.locateBtnFormatter = className(this.blockName, 'locate-btn');
        this.addressesDropdownFormatter = className(this.blockName, 'addresses-dd');
    }

    public render(): React.ReactNode {
        if (!this.controller.quickAddressSearchWindowStore.isShow) {
            return null;
        }

        const theme = this.theme;

        // TODO: replace more useful dropdown
        const items = this.controller.quickAddressSearchWindowStore.items.map(
            (item: Address): React.ReactNode => (
                <span
                    key={item.id}
                    style={{ cursor: 'pointer' }}
                    data-item={item.name}
                    className="dropdown-item"
                    onClick={this.controller.selectItem}
                >
                    {item.name}
                </span>
            ),
        );
        const resultsDropdown =
            this.controller.quickAddressSearchWindowStore.items.length !== 0 ? (
                <div className="dropdown-menu" style={{ display: 'unset', width: '100%' }}>
                    {items}
                </div>
            ) : null;

        return (
            <Window blockName={this.blockName} elementName="dialog" draggableHandler="header">
                <WindowHeader blockName={this.blockName} elementName="header">
                    <h5 className={this.titleFormatter({ theme })}>
                        {this.props.t('Quick address search')}
                    </h5>
                    <WindowHeaderButtonsContainer>
                        <WindowHeaderMinimize
                            blockName={this.blockName}
                            className="keypro--modal__default"
                            elementName="minimize"
                            action={this.controller.quickAddressSearchWindowStore.hide}
                        />
                        <WindowHeaderFavorite
                            blockName={this.blockName}
                            className="keypro--modal__green"
                            elementName="favorite"
                            action={this.controller.quickAddressSearchWindowStore.hide}
                        />
                        <WindowHeaderClose
                            blockName={this.blockName}
                            className="keypro--modal__red"
                            elementName="close"
                            action={this.controller.quickAddressSearchWindowStore.hide}
                        />
                    </WindowHeaderButtonsContainer>
                </WindowHeader>
                <WindowBody blockName={this.blockName} elementName="body">
                    <div className="row">
                        <div className="col-12">
                            <div className="input-group">
                                <input
                                    type="text"
                                    className={`${this.searchInputFormatter()} form-control`}
                                    placeholder="Input address"
                                    value={this.controller.quickAddressSearchWindowStore.pattern}
                                    onChange={this.controller.loadAddressesByPattern}
                                />
                                {resultsDropdown}
                                <div className="input-group-append">
                                    <button
                                        className="btn btn-outline-secondary"
                                        type="button"
                                        onClick={this.controller.locateToPoint}
                                    >
                                        <i className="fas fa-search-location" />
                                    </button>
                                    <button className="btn btn-outline-secondary" type="button">
                                        <i className="fas fa-binoculars" />
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </WindowBody>
            </Window>
        );
    }
}

export const QuickAddressSearchWindow = withTranslation()(Component);
