import { IActionElementProps } from '../../../props/element-props';

export interface ITopBarButtonProps extends IActionElementProps<any> {
    float?: string;
    id?: string;
}
