import { IBlockProps } from './block-props';

export interface IControllerProps<T> extends IBlockProps {
    controller: T;
}
