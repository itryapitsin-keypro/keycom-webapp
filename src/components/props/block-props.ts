import { IThemedProps } from './themed-props';

export interface IBlockProps extends IThemedProps {
    blockName: string;
}
