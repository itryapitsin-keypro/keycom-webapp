import * as React from 'react';
import { TopBar as BaseTopBar, TopBarButton } from '../top-bar';
import { QuickAddressSearchWindow } from '../windows/quick-address-search/component';
import { IApp } from './props';

export class App extends React.Component<IApp> {
    public render(): React.ReactNode {
        return (
            <div className="app">
                <BaseTopBar blockName="top-bar">
                    <TopBarButton
                        blockName="top-bar"
                        elementName="quick-address-search-window-show-btn"
                        action={this.props.topBarController.showHelloWorld}
                    >
                        <i className="far fa-comment-alt" />
                    </TopBarButton>
                    <TopBarButton
                        blockName="top-bar"
                        elementName="hello-world-btn"
                        action={this.props.topBarController.showQuickAddressSearch}
                    >
                        <i className="far fa-lightbulb" />
                    </TopBarButton>
                </BaseTopBar>
                <div className="map" />
                <div className="footer" />
                <QuickAddressSearchWindow
                    controller={this.props.quickAddressSearchWindowController}
                    blockName="quick-address-search"
                />
            </div>
        );
    }
}
