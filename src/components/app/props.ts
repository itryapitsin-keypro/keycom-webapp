import { TopBarController } from '../../controllers/top-bar';
import { QuickAddressSearchWindowController } from '../../controllers/windows/quick-address-search-window';

export interface IApp {
    topBarController: TopBarController;

    quickAddressSearchWindowController: QuickAddressSearchWindowController;
}
