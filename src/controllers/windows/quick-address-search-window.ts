import * as React from 'react';
import { inject, injectable } from 'inversify';
import { IQuickAddressSearchService } from '../../api/services/quick-address-search-service';
import { QuickAddressSearchWindowStore } from '../../stores/windows/quick-address-search-window-store';
import { Address } from '../../api/models/address';
import { MINIMUM_CHARS_FOR_SEARCHING } from '../../utils/settings';

@injectable()
export class QuickAddressSearchWindowController {
    private readonly _quickAddressSearchService: IQuickAddressSearchService;
    private readonly _quickAddressSearchWindowStore: QuickAddressSearchWindowStore;

    public constructor(
        @inject('QuickAddressSearchService') quickAddressSearchService: IQuickAddressSearchService,
        @inject(QuickAddressSearchWindowStore)
        quickAddressSearchWindowStore: QuickAddressSearchWindowStore,
    ) {
        this._quickAddressSearchService = quickAddressSearchService;
        this._quickAddressSearchWindowStore = quickAddressSearchWindowStore;
    }

    public selectItem = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>): void => {
        const item = this.quickAddressSearchWindowStore.items.find(
            (x: Address): boolean =>
                x.name === e.currentTarget.attributes.getNamedItem('data-item').value,
        );

        this.quickAddressSearchWindowStore.selectItem(item);
        this.quickAddressSearchWindowStore.setItems([]);
    };

    public loadAddressesByPattern = async (e: React.FormEvent<HTMLInputElement>): Promise<void> => {
        const pattern = e.currentTarget.value;
        this.quickAddressSearchWindowStore.setPatter(pattern);

        if (pattern.length < MINIMUM_CHARS_FOR_SEARCHING) {
            return;
        }

        this._quickAddressSearchWindowStore.startLoading();

        const data = await this._quickAddressSearchService
            .getAddressesByPattern(pattern)
            .finally(this._quickAddressSearchWindowStore.stopLoading);

        this._quickAddressSearchWindowStore.setItems(data.items);
    };

    public get quickAddressSearchService(): IQuickAddressSearchService {
        return this._quickAddressSearchService;
    }

    public get quickAddressSearchWindowStore(): QuickAddressSearchWindowStore {
        return this._quickAddressSearchWindowStore;
    }

    public locateToPoint = (): void => {
        if (!this.quickAddressSearchWindowStore.selectedItem) {
            return;
        }

        this.quickAddressSearchService.locateToPoint(
            this.quickAddressSearchWindowStore.selectedItem.lat,
            this.quickAddressSearchWindowStore.selectedItem.lon,
        );
    };
}
