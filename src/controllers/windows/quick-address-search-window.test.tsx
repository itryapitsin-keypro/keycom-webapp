import React from 'react';

import { Container } from 'inversify';
import { StoreModule } from '../../stores/module';
import { ApiModule } from '../../api/module';
import { ControllerModule } from '../module';
import { QuickAddressSearchWindowController } from './quick-address-search-window';

beforeEach((): void => {});

afterEach((): void => {});

it('testing QuickAddressSearchWindowController', async (): Promise<void> => {
    const di = new Container();
    di.load(StoreModule, ApiModule, ControllerModule);
    const quickAddressSearchWindowController = di.get(QuickAddressSearchWindowController);

    expect(quickAddressSearchWindowController).toBeDefined();
});
